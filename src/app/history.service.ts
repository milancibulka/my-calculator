import { Subject, Observable } from 'rxjs';


export class HistoryService {
    private subject = new Subject<any>();
    public getHistoryItem = this.subject.asObservable();

    add(message: string, forDisplay: boolean) {
        this.subject.next({text: message, display: forDisplay});
    }
}

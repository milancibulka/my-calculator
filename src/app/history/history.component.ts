import { Component, OnInit, OnDestroy } from '@angular/core';
import { HistoryService } from '../history.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnDestroy {
  history: any[] = [];
  subscription: Subscription;

  constructor(private historyService: HistoryService) {

    this.subscription = this.historyService.getHistoryItem.subscribe(message => {
      if (message) {
        if (!message.display) {
          this.history.unshift(message);
        }
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}

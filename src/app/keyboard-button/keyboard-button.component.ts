import { Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'app-keyboard-button',
  templateUrl: './keyboard-button.component.html',
  styleUrls: ['./keyboard-button.component.css'],
})
export class KeyboardButtonComponent implements OnInit {
  @Input() name: string;

  constructor() { }

  ngOnInit() {
  }


}

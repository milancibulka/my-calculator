import { Component, OnInit } from '@angular/core';
import { CalculationService } from '../calculation.service';

@Component({
  selector: 'app-keyboard',
  templateUrl: './keyboard.component.html',
  styleUrls: ['./keyboard.component.css'],
  providers: [CalculationService]
})
export class KeyboardComponent implements OnInit {

  constructor(private calculationService: CalculationService) { }

  ngOnInit() {
  }

  onNumberButtonClicked(num: number) {
    this.calculationService.inputNumber(num);
  }

  onSignButtonClicked(sign: string) {
    this.calculationService.setSign(sign);
  }

  calculate() {
    this.calculationService.calculate();
  }

  negate() {
    this.calculationService.negate();
  }

  dot() {
    this.calculationService.dot();
  }

  allClear() {
    this.calculationService.allClear();
  }
}

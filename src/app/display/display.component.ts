import { Component, OnInit, OnDestroy } from '@angular/core';
import { HistoryService } from '../history.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css']
})
export class DisplayComponent implements OnDestroy {
  result = '0';
  subscription: Subscription;

  constructor(private historyService: HistoryService) {
    this.subscription = this.historyService.getHistoryItem.subscribe(message => {
      if (message) {
        if (message.display) {
          this.result = message.text;
        }
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}

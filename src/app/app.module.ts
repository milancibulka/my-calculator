import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CalculatorComponent } from './calculator/calculator.component';
import { HistoryComponent } from './history/history.component';
import { DisplayComponent } from './display/display.component';
import { KeyboardComponent } from './keyboard/keyboard.component';
import { KeyboardButtonComponent } from './keyboard-button/keyboard-button.component';
import { HistoryService } from './history.service';

@NgModule({
  declarations: [
    AppComponent,
    CalculatorComponent,
    HistoryComponent,
    DisplayComponent,
    KeyboardComponent,
    KeyboardButtonComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [HistoryService],
  bootstrap: [AppComponent]
})
export class AppModule { }

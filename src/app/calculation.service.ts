import { HistoryService } from './history.service';
import { Injectable } from '@angular/core';

@Injectable()
export class CalculationService {

  constructor(private historyService: HistoryService) { }

  private result = 0;
  private input = 0;
  private display = this.result.toString();
  private sign = '';
  private decimalPlaces = 0.1;
  private lastButtonIsNumber = false;
  private signWasSet = false;
  private start = true;
  private dotWasPressed = false;
  private calculated = true;
  private  historyDisplay = '';

  inputNumber(input: number) {
    this.signWasSet = false;

    if (this.lastButtonIsNumber) {
      if (this.dotWasPressed) {
        this.input += input * this.decimalPlaces ;
        this.decimalPlaces /= 10;
      } else {
        this.input = (this.input * 10) + input;
      }
    } else {
      if (this.dotWasPressed) {
        this.input = input * this.decimalPlaces ;
        this.decimalPlaces /= 10;
      } else {
        this.input = input;
      }
    }
    this.input = parseFloat(this.input.toFixed(12));

    if (this.calculated) {
      this.result = this.input;
      this.display = this.input.toString();
    } else {
      this.display += input.toString();
    }

    this.lastButtonIsNumber = true;
    this.historyService.add(this.display, true);
  }

  setSign(sign: string) {
    this.dotWasPressed = false;
    this.decimalPlaces = 0.1;


    if (this.signWasSet) {
      this.display = this.display.slice(0, -3);
    } else {
      if (this.start) {
        this.result = this.input;
        this.start = false;
      } else if (!this.calculated) {
        this.calculate();
      }
    }
    this.lastButtonIsNumber = false;
    this.sign = sign;
    this.display += ' ' + sign + ' ';
    this.signWasSet = true;
    this.calculated = false;
    this.historyService.add(this.display, true);
  }

  calculate() {
    this.historyDisplay = this.display;
    this.signWasSet = false;
    this.dotWasPressed = false;
    this.decimalPlaces = 0.1;
    switch (this.sign) {
      case '÷': {
         this.result /= this.input;
         break;
      }
      case 'x': {
         this.result *= this.input;
         break;
      }
      case '-': {
         this.result -= this.input;
         break;
      }
      case '+': {
         this.result += this.input;
         break;
      }
      default: {
         console.log('Invalid choice');
         break;
      }
    }

    this.display = parseFloat(this.result.toFixed(12)).toString();
    this.lastButtonIsNumber = false;
    this.historyService.add(this.display, true);
    if (this.calculated) {
      this.historyDisplay += ' ' + this.sign + ' ';
      this.historyDisplay += this.input.toString();
    }
    this.historyDisplay += ' = ';
    this.historyDisplay += this.display;
    this.historyService.add(this.historyDisplay, false);
    this.calculated = true;
  }

  negate() {
    if (this.lastButtonIsNumber) {
      this.display = this.display.slice(0, -(+this.input.toString().length));
      this.input = this.input * -1;
      this.display +=  '(' + this.input.toString() + ')';
    } else {
      this.display = this.display.slice(0, -(+this.result.toString().length));
      this.result = this.result * -1;
      this.display +=  '(' + this.result.toString() + ')';
    }
    this.historyService.add(this.display, true);
  }

  dot() {
    if (!this.dotWasPressed) {
      this.dotWasPressed = true;
      if (this.lastButtonIsNumber) {
        this.display += '.';
      } else if (this.calculated) {
        this.display = '0.';
      } else {
        this.display += '0.';
      }
      this.historyService.add(this.display, true);
    }
  }

  allClear() {
  this.result = 0;
  this.input = 0;
  this.display = this.result.toString();
  this.sign = '';
  this.decimalPlaces = 0.1;
  this.lastButtonIsNumber = false;
  this.signWasSet = false;
  this.start = true;
  this.dotWasPressed = false;
  this.calculated = true;
  this.historyService.add(this.display, true);
  }
}
